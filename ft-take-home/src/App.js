import React from 'react';
import './App.css';
import ControlledCarousel from './Carousel';


class UserForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            email: "",
            password: "",
            occupations: [],
            states: [],
        }

        this.handleFieldChange = this.handleFieldChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleFieldChange(event) {
      const value = event.target.value;
      this.setState({[event.target.id]: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.occupations;
        delete data.states;
        
        const url = "https://frontend-take-home.fetchrewards.com/form";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            console.log("status: " + response.status)
            console.log(response)
            const cleared = {
              name: "",
              email: "",
              password: "",
              occupations: [],
              states: [],
            };
            this.setState(cleared);

            document.getElementById("popup_message_success").className = "alert alert-success popup_message mt-3";
            document.getElementById("options_button").className = "d-none";
            setTimeout(function () {
                document.getElementById("popup_message_success").className = "d-none";
                document.getElementById("options_button").className = "";
            }, 5000);
        }else{
            document.getElementById("popup_message_fail").className = "alert alert-danger popup_message mt-3";
            setTimeout(function () {
                document.getElementById("popup_message_fail").className = "d-none";
            }, 5000);
        }
    }

    async componentDidMount() {
      const url = "https://frontend-take-home.fetchrewards.com/form";
      const response = await fetch(url);
      if (response.ok) {
          const data = await response.json();
          this.setState({occupations: data.occupations});
          this.setState({states: data.states});
      };
    }
    
  
    render() {
        return (
            <div className="CreateUserPage shadow p-4">
                <div className="carousel_area">
                    <ControlledCarousel />
                </div>

                <div className="createUserForm">
                    <div className="">
                        <div className="">
                            <h3>Welcome!</h3>
                            <p>Thanks for choosing Fetch Rewards!</p>
                            <form className='option_area ' onSubmit={this.handleSubmit}>
                                <div className="options_div">
                                    <label className="options_label" htmlFor="name"><b>Full Name</b></label>
                                    <input value={this.state.name} onChange={this.handleFieldChange} placeholder="Full Name" required type="text" name="name" id="name" className="form-control" />
                                </div>
                                <div className="options_div">
                                    <label className="options_label" htmlFor="email"><b>Email</b></label>
                                    <input value={this.state.email} onChange={this.handleFieldChange} placeholder="Email" required type="email" name="email" id="email" className="form-control" />
                                </div>
                                <div className="options_div">
                                    <label className="options_label" htmlFor="password"><b>Password</b></label>
                                    <input value={this.state.password} onChange={this.handleFieldChange} placeholder="Password" required type="password" name="password" id="password" className="form-control" />
                                </div>

                                <div className="options_div">
                                    <label className="options_label" htmlFor="occupation"><b>Occupation</b></label>
                                    <select value={this.state.occupation} onChange={this.handleFieldChange} required name="occupation" id="occupation" className="form-select">
                                        <option value="">Choose an occupation</option>
                                        {this.state.occupations.map((occupation,index) => {
                                            return (
                                                <option key={index} value={occupation}>
                                                    {occupation}
                                                </option>
                                            );
                                        })}
                                    </select>
                                </div>
                        
                                <div className="options_div">
                                    <label className="options_label" htmlFor="state"><b>State</b></label>
                                    <select value={this.state.state} onChange={this.handleFieldChange} required name="state" id="state" className="form-select">
                                        <option value="">Choose a State</option>
                                        {this.state.states.map(state => {
                                            return (
                                                <option key={state.abbreviation} value={state.abbreviation}>
                                                    {state.name}
                                                </option>
                                            );
                                        })}
                                    </select>
                                </div>

                                <div id="options_button" className="">
                                    <button className="btn btn-primary mt-3 createButton"><b>CREATE</b></button>
                                </div>

                            </form>

                            <div id ="popup_message_success" className="d-none">
                                Congratulations! You just created a new user!
                            </div>
                            <div id ="popup_message_fail" className="d-none">
                                Sorry! Please try again...
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default UserForm;