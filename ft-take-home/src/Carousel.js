import Carousel from 'react-bootstrap/Carousel';
import { Component } from 'react';

export default class ControlledCarousel extends Component {
    constructor() {
        super();
        this.state = {
            img_list: ["./Fetch1.png", "./Fetch2.png", "./Fetch3.png"],
            index: 0
        };
        this.handleSelect = this.handleSelect.bind(this);
    }

    handleSelect(selectedIndex, e) {
        this.setState({ index: selectedIndex})
    }

    render() {
        return (
            <div>
                <Carousel activeIndex={this.state.index} onSelect={this.handleSelect}>
                    {this.state.img_list.map((FetchImage, index) => (
                            <Carousel.Item key={index} className="carousel-slide" >
                              
                                    <div className='mydiv'>
                                        <img className='slide-photo'
                                            src={FetchImage}
                                            alt="First slide"
                                        />
                                    </div>
                             
                            </Carousel.Item>
                    )
                )}
                </Carousel>
            </div>
        );
    }
}