# Fetch Rewards-Frontend Take-Home Exercise

A web form for create a new user.

## Getting started

- Get into ft-take-home directory
- Type 'npm start' in commend to start the server
- Go to 'localhost:3000' to see the webpage 

----

Please see below for the **Create User** preview.
<br>

![Semantic description of image](/ft-take-home/public/createuser.gif)
<br>